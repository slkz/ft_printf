#include "ft_printf.h"
#include <stdio.h>
#include <locale.h>

int		main()
{
	setlocale(LC_ALL, "");
	//ft_printf("ft{\n%024hho et%#1.2o %0012.O}\n", (unsigned char)12, 0, 123654789);
	//printf("og{\n%024hho et%#1.2o %0012.O}\n", (unsigned char)12, 0, 123654789);

//	ft_printf("ft{t %#7.5X}\n", 0xab);
//	printf("og{t %#7.5X}\n", 0xab);

	//ft_printf("ft{%#04X !!}\n", (unsigned short)0);
	//printf("og{%#04X !!}\n", (unsigned short)0);

	int	ret;

	ret = ft_printf("%C\n", 0x11ffff);
	ft_printf("%d\n", ret);
	ret = printf("%C\n", 0x11ffff);
	ft_printf("%d\n", ret);

	return (0);
}
