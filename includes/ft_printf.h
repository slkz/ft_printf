/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 18:12:52 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/15 06:36:30 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdio.h>
# include <stdint.h>
# include <stdarg.h>
# include <wchar.h>
# include <stdlib.h>
# include <unistd.h>

typedef enum	e_bool
{
	FALSE = 0,
	TRUE
}				t_bool;

typedef struct	s_modifiers
{
	t_bool	h;
	t_bool	hh;
	t_bool	l;
	t_bool	ll;
	t_bool	j;
	t_bool	z;
}				t_modifiers;

typedef struct	s_flags
{
	t_bool	hash;
	t_bool	zero;
	t_bool	minus;
	t_bool	plus;
	t_bool	space;
}				t_flags;

typedef struct	s_specifier
{
	t_bool	s;
	t_bool	ls;
	t_bool	p;
	t_bool	d;
	t_bool	ld;
	t_bool	i;
	t_bool	o;
	t_bool	lo;
	t_bool	u;
	t_bool	lu;
	t_bool	x;
	t_bool	lx;
	t_bool	c;
	t_bool	lc;
	t_bool	null;
	t_bool	n;
	t_bool	b;
}				t_specifier;

typedef	struct	s_global
{
	t_modifiers	modifiers;
	t_flags		flags;
	t_specifier	specifier;
	int			width;
	int			precision;
	int			arg;
	size_t		ret;
	int			allocated;
}				t_global;

/*
** libft
*/
int				ft_atoi(const char *str);
int				ft_isdigit(int c);
int				ft_ishexa(int c, t_global *global);
int				ft_islower(int c);
void			*ft_memset(void *s, int c, size_t n);
int				ft_toupper(int c);
void			ft_putchar(char c);
void			ft_putnstr(char const *s, size_t len);
void			ft_putstr(char const *s);
void			ft_putstrend(char *s);
char			*ft_strchr(char *str, int c);
char			*ft_strcpy(char *dst, const char *src);
int				ft_strcmp(const char *s1, const char *s2);
ssize_t			ft_strlen(const char *str);
void			ft_strrev(char *str);
char			*ft_strdup(char *s1);
void			ft_printaddr(void *ptr, char *buf);
int				conv_isnum(t_specifier specifier);

/*
** printf
*/
int				ft_printf(char *format, ...);

/*
** itoa_base
*/
void			itoa_base(intmax_t n, char *buf, int base, int precision);
void			uitoa_base(uintmax_t n, char *buf, int base, int precision);

/*
** init
*/
char			*init_flags(char *format, t_flags *flags);
char			*init_modifiers(char *format, t_modifiers *modifiers);
void			init_specifier(t_specifier *specifier);

/*
** init2
*/

char			*get_precision(char *loc, va_list ap, t_global *global);
char			*get_width(char *loc, t_global *global);
char			*free_buf(char *buf);
char			*init_buf(t_global *global);

/*
** flags
*/
void			ft_putflags(char *buf, t_global *global);

/*
** ft_printf_str
*/
void			ft_strtoupper(char *str);
void			ft_addprefixe(char *buf, char *prefixe, size_t start);
void			ft_fillspace(char *buf, char c);
void			ft_addpost(char *buf, char c);
void			ft_addpostnull(char *buf, char c, int width);

/*
** width
*/
int				adjust_width(char *buf, t_global *global);

/*
** precision
*/
void			adjust_precision(char *buf, t_global *global);

/*
** ft_putwchar
*/
int				ft_putwchar(wchar_t wc, char *buf);
void			ft_putwstr(wchar_t *wstr, char *buf);

/*
** printf_format
*/
void			printf_format_di(t_global *global, va_list ap,
		char *format, char *buf);
void			printf_format_u(t_global *global, va_list ap, char *format,
		char *buf);
void			printf_format_o(t_global *global, va_list ap, char *format,
		char *buf);
void			printf_format_x(t_global *global, va_list ap, char *format,
		char *buf);
int				printf_format_c(t_global *global, va_list ap, char *format,
		char *buf);
void			printf_format_s(t_global *global, va_list ap, char *format,
		char *buf);
void			printf_format_p(t_global *global, va_list ap, char *buf);
void			printf_format_n_b(t_global *global, va_list ap, char *format,
		char *buf);
void			apply_clcexception(char *buf, int width, t_global *global);
#endif
