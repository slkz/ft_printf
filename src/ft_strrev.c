/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 15:32:36 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/12/13 15:33:09 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_strrev(char *str)
{
	char	*tmp;
	size_t	size;
	size_t	i;
	size_t	j;

	if (!str)
		return ;
	size = ft_strlen(str);
	tmp = malloc(sizeof(char) * size + 1);
	i = 0;
	j = size - 1;
	while (i < size)
		tmp[i++] = str[j--];
	tmp[i] = '\0';
	str = ft_strcpy(str, tmp);
	free(tmp);
}
