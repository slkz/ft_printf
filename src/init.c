/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 16:55:51 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/01/23 19:31:33 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*set_n_skip(t_bool *modifiers, char *loc)
{
	*modifiers = TRUE;
	loc++;
	return (loc);
}

char		*init_flags(char *format, t_flags *flags)
{
	flags->hash = FALSE;
	flags->zero = FALSE;
	flags->minus = FALSE;
	flags->plus = FALSE;
	flags->space = FALSE;
	while (*format == '#' || *format == '0' || *format == '-' || *format == '+'
			|| *format == ' ')
	{
		if (*format == '#')
			flags->hash = TRUE;
		if (*format == '0')
			flags->zero = TRUE;
		if (*format == '-')
			flags->minus = TRUE;
		if (*format == '+')
			flags->plus = TRUE;
		if (*format == ' ')
			flags->space = TRUE;
		format++;
	}
	return (format);
}

static void	set_modifiers(t_modifiers *modifiers)
{
	modifiers->h = FALSE;
	modifiers->hh = FALSE;
	modifiers->l = FALSE;
	modifiers->ll = FALSE;
	modifiers->j = FALSE;
	modifiers->z = FALSE;
}

char		*init_modifiers(char *loc, t_modifiers *modifiers)
{
	set_modifiers(modifiers);
	while (*loc == 'h' || *loc == 'l' || *loc == 'j' || *loc == 'z')
	{
		if (*loc == 'h')
		{
			if (*(loc + 1) == 'h')
				loc = set_n_skip(&modifiers->hh, loc);
			else
				modifiers->h = TRUE;
		}
		if (*loc == 'l')
		{
			if (*(loc + 1) == 'l')
				loc = set_n_skip(&modifiers->ll, loc);
			else
				modifiers->l = TRUE;
		}
		*loc == 'j' ? modifiers->j = TRUE : 0;
		*loc == 'z' ? modifiers->z = TRUE : 0;
		loc++;
	}
	return (loc);
}

void		init_specifier(t_specifier *specifier)
{
	specifier->s = FALSE;
	specifier->ls = FALSE;
	specifier->p = FALSE;
	specifier->d = FALSE;
	specifier->ld = FALSE;
	specifier->i = FALSE;
	specifier->o = FALSE;
	specifier->lo = FALSE;
	specifier->u = FALSE;
	specifier->lu = FALSE;
	specifier->x = FALSE;
	specifier->lx = FALSE;
	specifier->c = FALSE;
	specifier->lc = FALSE;
	specifier->null = FALSE;
}
