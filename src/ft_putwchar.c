/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 16:05:06 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/02/05 19:45:20 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_putwchar(wchar_t wc, char *buf)
{
	char *ptr;

	ptr = buf;
	if (wc <= 0x7F)
		*ptr++ = wc;
	else if (wc <= 0x7FF)
	{
		*ptr++ = (wc >> 6) + 0xC0;
		*ptr++ = (wc & 0x3F) + 0x80;
	}
	else if (wc <= 0xFFFF)
	{
		*ptr++ = (wc >> 12) + 0xE0;
		*ptr++ = ((wc >> 6) & 0x3F) + 0x80;
		*ptr++ = (wc & 0x3F) + 0x80;
	}
	else if (wc <= 0x10FFFF)
	{
		*ptr++ = (wc >> 18) + 0xF0;
		*ptr++ = ((wc >> 12) & 0x3F) + 0x80;
		*ptr++ = ((wc >> 6) & 0x3F) + 0x80;
		*ptr++ = (wc & 0x3F) + 0x80;
	}
	return (ptr - buf);
}

void			ft_putwstr(wchar_t *wstr, char *buf)
{
	int ret;

	if (wstr == NULL)
	{
		ft_strcpy(buf, "(null)");
		return ;
	}
	while (*wstr)
	{
		ret = ft_putwchar(*wstr++, buf);
		buf += ret;
	}
}
