/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_format.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 16:07:22 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/13 01:44:05 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	printf_format_di(t_global *global, va_list ap, char *format, char *buf)
{
	if (*format == 'i')
		global->specifier.i = TRUE;
	else
		*format == 'D' ? (global->specifier.ld = TRUE) :
			(global->specifier.d = TRUE);
	if (global->modifiers.h == TRUE && global->specifier.ld == FALSE)
		itoa_base((short)va_arg(ap, int), buf, 10, global->precision);
	else if (global->modifiers.hh == TRUE && global->specifier.ld == FALSE)
		itoa_base((char)va_arg(ap, int), buf, 10, global->precision);
	else if (global->modifiers.l == TRUE || *format == 'D')
		itoa_base(va_arg(ap, long int), buf, 10, global->precision);
	else if (global->modifiers.ll == TRUE)
		itoa_base(va_arg(ap, long long int), buf, 10, global->precision);
	else if (global->modifiers.j == TRUE)
		itoa_base(va_arg(ap, intmax_t), buf, 10, global->precision);
	else if (global->modifiers.z == TRUE)
		itoa_base(va_arg(ap, size_t), buf, 10, global->precision);
	else
		itoa_base(va_arg(ap, int), buf, 10, global->precision);
}

void	printf_format_u(t_global *global, va_list ap, char *format, char *buf)
{
	*format == 'U' ? (global->specifier.lu = TRUE) :
		(global->specifier.u = TRUE);
	if (global->modifiers.h == TRUE && global->specifier.lu == FALSE)
		uitoa_base((unsigned short int)va_arg(ap, unsigned int), buf, 10,
				global->precision);
	else if (global->modifiers.hh == TRUE && global->specifier.lu == FALSE)
		uitoa_base((unsigned char)va_arg(ap, unsigned int), buf, 10,
				global->precision);
	else if (global->modifiers.l == TRUE || *format == 'U')
		uitoa_base(va_arg(ap, unsigned long int), buf, 10,
				global->precision);
	else if (global->modifiers.ll == TRUE)
		uitoa_base(va_arg(ap, unsigned long long int), buf, 10,
				global->precision);
	else if (global->modifiers.j == TRUE)
		uitoa_base(va_arg(ap, uintmax_t), buf, 10, global->precision);
	else if (global->modifiers.z == TRUE)
		uitoa_base(va_arg(ap, size_t), buf, 10, global->precision);
	else
		uitoa_base(va_arg(ap, unsigned int), buf, 10, global->precision);
}

void	printf_format_o(t_global *global, va_list ap, char *format, char *buf)
{
	*format == 'O' ? (global->specifier.lo = TRUE) :
		(global->specifier.o = TRUE);
	if (global->modifiers.h == TRUE && global->specifier.lo == FALSE)
		uitoa_base((unsigned short int)va_arg(ap, unsigned int), buf, 8,
				global->precision);
	else if (global->modifiers.hh == TRUE && global->specifier.lo == FALSE)
		uitoa_base((unsigned char)va_arg(ap, unsigned int), buf, 8,
				global->precision);
	else if (global->modifiers.l == TRUE || *format == 'O')
		uitoa_base(va_arg(ap, unsigned long int), buf, 8,
				global->precision);
	else if (global->modifiers.ll == TRUE)
		uitoa_base(va_arg(ap, unsigned long long int), buf, 8,
				global->precision);
	else if (global->modifiers.j == TRUE)
		uitoa_base(va_arg(ap, uintmax_t), buf, 8, global->precision);
	else if (global->modifiers.z == TRUE)
		uitoa_base(va_arg(ap, size_t), buf, 8, global->precision);
	else
		uitoa_base(va_arg(ap, unsigned int), buf, 8, global->precision);
	if (buf[0] == '0')
		global->arg = 0;
}

void	printf_format_x(t_global *global, va_list ap, char *format, char *buf)
{
	*format == 'X' ? (global->specifier.lx = TRUE) :
		(global->specifier.x = TRUE);
	if (global->modifiers.h == TRUE)
		uitoa_base((unsigned short int)va_arg(ap, unsigned int), buf, 16,
				global->precision);
	else if (global->modifiers.hh == TRUE)
		uitoa_base((unsigned char)va_arg(ap, unsigned int), buf, 16,
				global->precision);
	else if (global->modifiers.l == TRUE)
		uitoa_base(va_arg(ap, unsigned long int), buf, 16,
				global->precision);
	else if (global->modifiers.ll == TRUE)
		uitoa_base(va_arg(ap, unsigned long long int), buf, 16,
				global->precision);
	else if (global->modifiers.j == TRUE)
		uitoa_base(va_arg(ap, uintmax_t), buf, 16, global->precision);
	else if (global->modifiers.z == TRUE)
		uitoa_base(va_arg(ap, size_t), buf, 16, global->precision);
	else
		uitoa_base(va_arg(ap, unsigned int), buf, 16, global->precision);
	if (global->specifier.lx == TRUE)
		ft_strtoupper(buf);
	if (buf[0] == '0')
		global->arg = 0;
}

void	printf_format_p(t_global *global, va_list ap, char *buf)
{
	global->specifier.p = TRUE;
	ft_printaddr(va_arg(ap, void *), buf);
}
