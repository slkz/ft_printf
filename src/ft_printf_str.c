/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_str.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 15:29:09 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/02/05 19:28:21 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_strtoupper(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		str[i] = ft_toupper(str[i]);
		i++;
	}
}

void	ft_addprefixe(char *buf, char *prefixe, size_t start)
{
	size_t		buf_len;
	size_t		prefixe_len;
	size_t		max;

	if (start != 0)
		buf = buf + start;
	prefixe_len = ft_strlen(prefixe);
	buf_len = ft_strlen(buf);
	max = prefixe_len + buf_len;
	buf[max--] = '\0';
	buf_len -= 1;
	prefixe_len -= 1;
	while (max >= buf_len)
		buf[max--] = buf[buf_len--];
	while (max != 0)
		buf[max--] = prefixe[prefixe_len--];
	buf[max] = prefixe[prefixe_len];
}

void	ft_fillspace(char *buf, char c)
{
	int	i;

	i = 0;
	while (buf[i] == ' ')
		buf[i++] = c;
}

void	ft_addpost(char *buf, char c)
{
	int		end;

	end = ft_strlen(buf);
	buf[end++] = c;
	buf[end] = '\0';
}

void	ft_addpostnull(char *buf, char c, int width)
{
	int		i;

	i = 1;
	while (i < width)
		buf[i++] = c;
	buf[i] = '\0';
}
