/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_format_alpha.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 19:39:10 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/15 06:30:52 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		printf_format_c(t_global *global, va_list ap, char *format, char *buf)
{
	*format == 'C' ? (global->specifier.lc = TRUE) :
		(global->specifier.c = TRUE);
	if (global->modifiers.l == TRUE || global->specifier.lc == TRUE)
		ft_putwchar(global->arg = va_arg(ap, wint_t), buf);
	else
	{
		global->arg = va_arg(ap, int);
		buf[0] = global->arg;
	}
	return (global->arg);
}

void	printf_format_s(t_global *global, va_list ap, char *format, char *buf)
{
	*format == 'S' ? (global->specifier.ls = TRUE) :
		(global->specifier.s = TRUE);
	if (global->modifiers.l || *format == 'S')
		ft_putwstr(va_arg(ap, wchar_t *), buf);
	else
		ft_strcpy(buf, va_arg(ap, char *));
}

void	printf_format_n_b(t_global *global, va_list ap, char *format, char *buf)
{
	int		*n;

	if (*format == 'n')
	{
		n = va_arg(ap, int *);
		*n = global->ret;
	}
	else
	{
		global->specifier.b = TRUE;
		itoa_base(va_arg(ap, int), buf, 2, global->precision);
	}
}

void	apply_clcexception(char *buf, int width, t_global *global)
{
	ft_putstrend(buf);
	if (width == 42)
	{
		ft_putstr(buf + 1);
		global->ret += ft_strlen(buf + 1);
	}
	global->ret += 1;
}
