/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   width.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 17:17:50 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/15 06:36:29 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	apply_width(char *buf, t_global *global)
{
	if (global->flags.space == TRUE &&
			ft_strlen(buf) == global->width - 1)
		ft_addprefixe(buf, " ", 0);
	else if (buf[0] == '-' || buf[0] == '+')
		ft_addprefixe(buf, "0", 1);
	else if (global->flags.hash == TRUE && global->arg != 0)
		ft_addprefixe(buf, "0", 2);
	else if (global->specifier.p == TRUE)
		ft_addpost(buf, '0');
	else
		ft_addprefixe(buf, "0", 0);
}

static int	check_specifier(t_global *global)
{
	if (global->flags.minus == FALSE && global->flags.zero == TRUE &&
			((conv_isnum(global->specifier) && global->precision < 0) ||
			global->specifier.c == TRUE || global->specifier.s == TRUE ||
			global->specifier.p == TRUE || global->specifier.null == TRUE ||
			global->specifier.ls == TRUE || global->specifier.null == TRUE) &&
			global->precision <= 0)
		return (1);
	return (0);
}

int			adjust_width(char *buf, t_global *global)
{
	int	i;

	i = 0;
	(global->specifier.c == TRUE && buf[0] == 0) ? i = 1 : 0;
	while (ft_strlen(buf) + i < global->width)
	{
		if (global->flags.minus == TRUE)
		{
			if (buf[0] == '\0' && global->specifier.c == TRUE)
			{
				ft_addpostnull(buf, ' ', global->width);
				return (42);
			}
			ft_addpost(buf, ' ');
		}
		else if (check_specifier(global))
			apply_width(buf, global);
		else if (global->flags.minus == FALSE &&
				global->flags.zero == TRUE &&
				global->specifier.null == TRUE)
			apply_width(buf, global);
		else
			ft_addprefixe(buf, " ", 0);
	}
	return (0);
}
