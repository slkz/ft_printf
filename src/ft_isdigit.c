/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 13:36:24 by lcuzzuco          #+#    #+#             */
/*   Updated: 2018/02/05 19:49:44 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

int		ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

int		ft_ishexa(int c, t_global *global)
{
	if (c >= '0' && c <= '9')
		return (1);
	else if (global->specifier.x == TRUE || global->specifier.lx == TRUE)
	{
		if ((c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f'))
			return (1);
	}
	return (0);
}

int		conv_isnum(t_specifier specifier)
{
	if (specifier.d == TRUE || specifier.ld == TRUE || specifier.i == TRUE ||
			specifier.o == TRUE || specifier.lo == TRUE ||
			specifier.u == TRUE || specifier.lu == TRUE ||
			specifier.x == TRUE || specifier.lx == TRUE)
		return (1);
	return (0);
}

int		ft_islower(int c)
{
	if ('a' <= c && c <= 'z')
		return (1);
	return (0);
}
