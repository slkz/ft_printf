/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printaddr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 15:27:50 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/12/13 15:27:51 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_printaddr(void *ptr, char *buf)
{
	int		i;
	int		j;
	char	*map;
	size_t	n;

	if (ptr == NULL)
	{
		ft_strcpy(buf, "0x0");
		return ;
	}
	n = (size_t)ptr;
	map = "0123456789abcdef";
	i = 40;
	j = 0;
	while (n > 0 && i > 0)
	{
		buf[i--] = map[n % 16];
		n /= 16;
	}
	buf[j++] = '0';
	buf[j++] = 'x';
	while (++i <= 40)
		buf[j++] = buf[i];
	buf[j] = '\0';
}
