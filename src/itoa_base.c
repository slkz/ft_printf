/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa_base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 16:49:10 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/01/23 16:06:54 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static intmax_t	set_neg(intmax_t n, t_bool *negative)
{
	*negative = TRUE;
	return (-n);
}

void			itoa_base(intmax_t n, char *buf, int base, int precision)
{
	int		i;
	int		rem;
	t_bool	negative;

	i = 0;
	negative = FALSE;
	if ((long unsigned int)n == -9223372036854775808u)
		ft_strcpy(buf, "-9223372036854775808");
	else if (precision == 0 && n == 0)
		buf[0] = 0;
	else
	{
		if (n == 0)
			buf[i++] = '0';
		n < 0 && base == 10 ? n = set_neg(n, &negative) : 0;
		while (n != 0)
		{
			rem = n % base;
			buf[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
			n /= base;
		}
		negative ? buf[i++] = '-' : 0;
		buf[i] = '\0';
		ft_strrev(buf);
	}
}

void			uitoa_base(uintmax_t n, char *buf, int base, int precision)
{
	int	i;
	int	rem;

	i = 0;
	if (precision == 0 && n == 0)
	{
		buf[0] = 0;
		return ;
	}
	if (n == 0)
		buf[i++] = '0';
	while (n != 0)
	{
		rem = n % base;
		buf[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
		n /= base;
	}
	buf[i] = '\0';
	ft_strrev(buf);
}
