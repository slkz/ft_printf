/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 15:08:03 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/02/03 17:33:53 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	conv_isunsigned(t_specifier specifier)
{
	if (specifier.u == TRUE || specifier.lu == TRUE || specifier.o == TRUE ||
			specifier.lo == TRUE || specifier.x == TRUE ||
			specifier.lx == TRUE)
		return (1);
	return (0);
}

void		ft_putflags(char *buf, t_global *global)
{
	if (global->flags.hash == TRUE)
	{
		if ((global->specifier.o == TRUE || global->specifier.lo == TRUE) &&
				ft_strcmp(buf, "0"))
			ft_addprefixe(buf, "0", 0);
		if ((global->specifier.x == TRUE || global->specifier.lx == TRUE) &&
				buf[0] != '0' && buf[0] != 0)
			global->specifier.x == TRUE ? ft_addprefixe(buf, "0x", 0) :
				ft_addprefixe(buf, "0X", 0);
	}
	if (global->flags.zero == TRUE)
	{
		if (conv_isnum(global->specifier) && global->precision == -1)
			global->flags.minus == FALSE ? ft_fillspace(buf, '0') : 0;
	}
	if (global->flags.space == TRUE && global->flags.plus == FALSE &&
			global->width == 0)
		(global->specifier.d == TRUE || global->specifier.ld == TRUE ||
					global->specifier.i == TRUE) && buf[0] != '-' ?
			ft_addprefixe(buf, " ", 0) : 0;
	if (global->flags.plus == TRUE)
		(conv_isnum(global->specifier)) && buf[0] != '-' &&
				!conv_isunsigned(global->specifier) ?
				ft_addprefixe(buf, "+", 0) : 0;
}
