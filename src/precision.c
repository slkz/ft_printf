/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   precision.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 17:13:52 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/13 06:06:48 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static ssize_t	ft_strlend(char *buf, t_global *global)
{
	ssize_t	digits;

	digits = 0;
	if ((global->specifier.x == TRUE || global->specifier.lx == TRUE) &&
			global->flags.hash == TRUE && (buf[1] == 'x' || buf[1] == 'X'))
		buf += 2;
	while (*buf)
	{
		if (ft_ishexa(*buf, global))
			digits++;
		buf++;
	}
	return (digits);
}

static int		find_len(int i, unsigned int o)
{
	if (o <= 0x7F)
		return (i += 1);
	else if (o >= 49280 && o < 14712960)
		return (i += 2);
	else if (o >= 14712960 && o < 4034953344)
		return (i += 3);
	else if (o > 4034953344)
		return (i += 4);
	return (-1);
}

static void		adapt_precision(t_global *global, char *buf)
{
	int		i;
	int		tmp;

	i = 0;
	tmp = global->precision;
	if (global->specifier.ls == TRUE)
	{
		while (i < tmp)
			i = find_len(i, (unsigned int)buf[global->precision]);
		if ((unsigned int)buf[global->precision] > 0x7F &&
				global->precision != 0)
			i--;
		buf[i] = '\0';
		return ;
	}
	buf[global->precision] = '\0';
}

void			adjust_precision(char *buf, t_global *global)
{
	if (global->specifier.p == TRUE && global->precision == 0 &&
			!ft_strcmp(buf, "0x0"))
		buf[2] = '\0';
	if (conv_isnum(global->specifier) || global->specifier.p == TRUE)
	{
		if (global->specifier.p == TRUE && !ft_strcmp(buf, "0x0"))
			global->precision++;
		while (ft_strlend(buf, global) < global->precision)
		{
			if (buf[0] == '-' || buf[0] == '+')
				ft_addprefixe(buf, "0", 1);
			else if ((global->specifier.p == TRUE) ||
					((global->specifier.x == TRUE ||
					global->specifier.lx == TRUE) &&
					global->flags.hash == TRUE && (buf[1] == 'x' ||
						buf[1] == 'X')))
				ft_addprefixe(buf, "0", 2);
			else
				ft_addprefixe(buf, "0", 0);
		}
	}
	else if (global->specifier.p == TRUE || global->specifier.s == TRUE ||
			global->specifier.ls == TRUE)
		adapt_precision(global, buf);
}
