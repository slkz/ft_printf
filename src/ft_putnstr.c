/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 15:31:23 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/12/13 15:31:37 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putnstr(char const *s, size_t len)
{
	size_t		i;

	i = 0;
	while (s[i] && len)
	{
		ft_putchar(s[i]);
		i++;
		len--;
	}
}
