/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 19:49:56 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/15 06:19:54 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*get_precision(char *loc, va_list ap, t_global *global)
{
	loc++;
	global->precision = 0;
	*loc >= '0' && *loc <= '9' ? global->precision = ft_atoi(loc++) : 0;
	while (*loc >= '0' && *loc <= '9')
		loc++;
	if (*loc == '*')
	{
		global->precision = va_arg(ap, int);
		loc++;
	}
	return (loc);
}

char	*get_width(char *loc, t_global *global)
{
	global->width = ft_atoi(loc);
	while (*loc >= '0' && *loc <= '9')
		loc++;
	return (loc);
}

char	*free_buf(char *buf)
{
	free(buf);
	return (NULL);
}

char	*init_buf(t_global *global)
{
	char			*buf;
	intmax_t		size;
	static char		buf_small[50];

	buf = NULL;
	global->allocated = 0;
	if (global->width > 25 || global->precision > 25)
	{
		size = (global->width + global->precision) + 50;
		if (!(buf = malloc(sizeof(char) * size)))
			return (NULL);
		ft_memset(buf, 0, size);
		global->allocated = 1;
		return (buf);
	}
	ft_memset(buf_small, 0, 50);
	return (buf_small);
}
