/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 16:08:36 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/15 06:31:16 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*init_conversion(char *loc, va_list ap, t_global *global)
{
	init_specifier(&global->specifier);
	global->width = 0;
	global->precision = -1;
	loc = init_flags(loc, &global->flags);
	*loc >= '0' && *loc <= '9' ? global->width = ft_atoi(loc) : 0;
	while (*loc >= '0' && *loc <= '9')
		loc++;
	if (*loc == '*')
	{
		global->width = va_arg(ap, int);
		if (global->width < 0)
		{
			global->width = -global->width;
			global->flags.minus = TRUE;
		}
		loc++;
	}
	if (*loc == '.')
		loc = get_precision(loc, ap, global);
	else if (*loc >= '0' && *loc <= '9')
		loc = get_width(loc, global);
	loc = init_modifiers(loc, &global->modifiers);
	return (loc);
}

static int	printf_format(t_global *global, va_list ap, char *format, char *buf)
{
	if (*format == '%')
	{
		global->specifier.null = TRUE;
		buf[0] = '%';
		buf[1] = '\0';
	}
	else if (*format == 'c' || *format == 'C')
		global->arg = printf_format_c(global, ap, format, buf);
	else if (*format == 'd' || *format == 'i' || *format == 'D')
		printf_format_di(global, ap, format, buf);
	else if (*format == 'u' || *format == 'U')
		printf_format_u(global, ap, format, buf);
	else if (*format == 's' || *format == 'S')
		printf_format_s(global, ap, format, buf);
	else if (*format == 'o' || *format == 'O')
		printf_format_o(global, ap, format, buf);
	else if (*format == 'p')
		printf_format_p(global, ap, buf);
	else if (*format == 'x' || *format == 'X')
		printf_format_x(global, ap, format, buf);
	else if (*format == 'n' || *format == 'b')
		printf_format_n_b(global, ap, format, buf);
	else
		return (0);
	return (1);
}

static char	*printf_format_other(t_global *global, char *format,
		char *buf)
{
	global->specifier.null = TRUE;
	buf[0] = *format++;
	global->width > 0 ? adjust_width(buf, global) : 0;
	global->ret += ft_strlen(buf);
	ft_putstr(buf);
	return (format);
}

static char	*printf_conversion(char *format, va_list ap, t_global *global)
{
	char			*buf;
	int				width;

	global->arg = -1;
	buf = init_buf(global);
	if (!printf_format(global, ap, format, buf))
	{
		global->allocated == 1 ? buf = free_buf(buf) : 0;
		return (printf_format_other(global, format, buf));
	}
	ft_putflags(buf, global);
	global->precision >= 0 ? adjust_precision(buf, global) : 0;
	global->width > 0 ? width = adjust_width(buf, global) : 0;
	global->ret += ft_strlen(buf);
	if (global->arg == 0 && (global->specifier.c == TRUE ||
				global->specifier.lc == TRUE))
		apply_clcexception(buf, width, global);
	else
		ft_putstr(buf);
	global->allocated == 1 ? buf = free_buf(buf) : 0;
	return (++format);
}

int			ft_printf(char *format, ...)
{
	va_list		ap;
	char		*loc;
	t_global	global;

	if (ft_strcmp(format, "%") == 0)
		return (0);
	global.ret = 0;
	va_start(ap, format);
	while ((loc = ft_strchr(format, '%')) != NULL)
	{
		ft_putnstr(format, loc - format);
		global.ret += loc - format;
		loc++;
		format = init_conversion(loc, ap, &global);
		format = printf_conversion(format, ap, &global);
	}
	global.ret += ft_strlen(format);
	ft_putstr(format);
	va_end(ap);
	return (global.ret > 0 ? global.ret : 0);
}
