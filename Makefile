NAME = libftprintf.a

INC_DIR = -Iincludes/
SRC_DIR = src/

SRCS = flags.c\
	   ft_atoi.c\
	   ft_isdigit.c\
	   ft_memset.c\
	   ft_printaddr.c\
	   ft_printf.c\
	   ft_printf_str.c\
	   ft_putchar.c\
	   ft_putnstr.c\
	   ft_putstr.c\
	   ft_putwchar.c\
	   ft_strchr.c\
	   ft_strcmp.c\
	   ft_strcpy.c\
	   ft_strlen.c\
	   ft_strdup.c\
	   ft_strrev.c\
	   ft_toupper.c\
	   init.c\
	   init_2.c\
	   itoa_base.c\
	   precision.c\
	   width.c\
	   printf_format_num.c\
	   printf_format_alpha.c\

OBJ = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)

$(OBJ):
	gcc -c -Wall -Werror -Wextra $(INC_DIR) $(addprefix $(SRC_DIR), $(SRCS))

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

test:
	gcc -w -Wall -Wextra $(INC_DIR) main.c $(addprefix $(SRC_DIR), $(SRCS)) && ./a.out | cat -e && rm ./a.out

testwchar:
	gcc -g -w -Wall -Wextra $(INC_DIR) main.c $(addprefix $(SRC_DIR), $(SRCS)) -o test_wchar && ./test_wchar

re: fclean all

.PHONY: all clean fclean re
